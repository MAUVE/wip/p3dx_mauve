# P3DX MAUVE Architectures

## p3dx_teleop_mauve

### Components

* `driver`: Aria driver component
* `teleop`: Teleoperation node

### MAUVE Architecture

```
digraph p3dx_teleop {
  teleop -> cmd -> driver;
}
```

Tips: to create pdf: `dot -Tpdf -O`

### ROS interface

* `teleop` inputs:
  * `/joy`
* `driver` outputs:
  * `/robot/pose`
  * `/robot/velocity`
  * `/robot/time`
  * `/robot/battery`

## p3dx_safecontrol_mauve

### Components

* `driver`: Aria driver component
* `teleop`: Teleoperation node
* `hokuyo`: Hokuyo driver component
* `safety`: Safety pilot
* `control`: Potential field controller

### MAUVE Architecture

```
digraph p3dx_safecontrol {
  teleop -> cmd -> driver;
  hokuyo -> scan -> safety;
  scan -> control;
  control -> command -> safety -> cmd;
}
```

Tips: to create pdf: `dot -Tpdf -O`

### ROS interface

* `teleop` inputs:
  * `/joy`
* `control` inputs:
  * `/target`
* `control` outputs:
  * `/obstacle`
* `hokuyo` outputs:
  * `/robot/scan`
* `driver` outputs:
  * `/robot/pose`
  * `/robot/velocity`
  * `/robot/time`
  * `/robot/battery`
