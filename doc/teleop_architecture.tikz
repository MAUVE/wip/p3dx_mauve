\usetikzlibrary{shapes,automata}
\usetikzlibrary{arrows,patterns}

\tikzset{
	component/.style= {rounded corners=10, draw, very thick,
		minimum width=50, minimum height=40, rectangle split,
		rectangle split parts=2,shared},
	resource/.style= {draw, very thick,fill=black!10,
		minimum width=60, minimum height=30},
	ros/.style= {ellipse, draw},
	exec/.style={state},
	sync/.style={state,diamond,rounded corners=0},
	fsm/.style={scale=.5,transform shape,node distance=50,initial text=,->},
	topic/.style={->,>=stealth',thick,dashed},
	port/.style={very thick,shorten <=-1pt, shorten >=-1pt},
	readport/.style={port,
		triangle 60 - open triangle 60 reversed},
	writeport/.style={port,
		triangle 60  reversed - open triangle 60},
	callport/.style={port, triangle 60 - open triangle 60},
	mauve_ros/.style={pattern=dots, pattern color=blue!60!black!60},
	shared/.style={draw=blue!70!red!80!black!50},
	base/.style={draw=blue!90!black!80}
}

\def\PSM{
\begin{tikzpicture}[style=fsm]
	\node[initial, exec] (e) {E};
	\node[sync,right of=e] (s) {S};
	\path (e) edge[bend left] (s)
		(s) edge[bend left] (e);
	\end{tikzpicture}}

\begin{tikzpicture}[]
\node[component] (teleop) at (0,0) {\tt teleop
	\nodepart{two} 
	\PSM};
\node[resource] (driver) at (0,-3) {\tt driver};
\node[resource,shared] (joystick) at (-5,0) {\tt joystick};

\draw[writeport] (teleop.240) -- (driver.north -| teleop.240);
\draw[writeport] (teleop.300) -- (driver.north -| teleop.300);
\draw[readport] (teleop) -- (joystick);

\node[resource,mauve_ros] (joy_subscriber) at (-10,0) {\tt joy\_subscriber};
\node[ros] (joy) at (-15,0) {\tt /joy};
\path[topic] (joy) edge (joy_subscriber) ;
\draw[writeport] (joy_subscriber) edge (joystick) ;

\node[component,mauve_ros] (pose_publisher) at (-6,-6) {\tt pose \nodepart{two} \PSM};
\node[ros] (pose) at (-6,-8) {\tt /robot/pose};
\path[topic] (pose_publisher) edge (pose) ;
\draw[readport] (pose_publisher) |- (driver) ;

\node[component,mauve_ros] (vel_publisher) at (-2,-6) {\tt velocity \nodepart{two} \PSM};
\node[ros] (vel) at (-2,-8) {\tt /robot/velocity};
\path[topic] (vel_publisher) edge (vel) ;
\draw[readport] (vel_publisher) |- (driver) ;

\node[component,mauve_ros] (time_publisher) at (6,-6) {\tt time \nodepart{two} \PSM};
\node[ros] (time) at (6,-8) {\tt /robot/time};
\path[topic] (time_publisher) edge (time) ;
\draw[readport] (time_publisher) |- (driver) ;

\node[component,mauve_ros] (bat_publisher) at (2,-6) {\tt battery \nodepart{two} \PSM};
\node[ros] (battery) at (2,-8) {\tt /robot/battery};
\path[topic] (bat_publisher) edge (battery) ;
\draw[readport] (bat_publisher) |- (driver) ;

\end{tikzpicture}