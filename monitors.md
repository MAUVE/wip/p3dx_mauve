# MAUVE P3DX

## Monitors

* __aria__:
  * read_time constant when disconnected
* __hokuyo__:
  * size_error and connection_error ports
  * check value of scan (blind scan)
* __v4l__:
  * no image published at rate
  * image is completely black/white
* __unicycle_control__:
  * __teleop__: _stop_ and _command_ short cycles
  * __safety__: _input_ not null but _output_ null for a while
  * __control__: distance to goal

## Architectures
